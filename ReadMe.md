ScanAbs.py [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
===============
~~[wheel (GitLab)](https://gitlab.com/KOLANICH/ScanAbs.py/-/jobs/artifacts/master/raw/dist/ScanAbs-0.CI-py3-none-any.whl?job=build)~~
~~[wheel (GHA via `nightly.link`)](https://nightly.link/KOLANICH-libs/ScanAbs.py/workflows/CI/master/ScanAbs-0.CI-py3-none-any.whl)~~
~~![GitLab Build Status](https://gitlab.com/KOLANICH-libs/ScanAbs.py/badges/master/pipeline.svg)~~
~~![GitLab Coverage](https://gitlab.com/KOLANICH-libs/ScanAbs.py/badges/master/coverage.svg)~~
~~[![GitHub Actions](https://github.com/KOLANICH-libs/ScanAbs.py/workflows/CI/badge.svg)](https://github.com/KOLANICH-libs/ScanAbs.py/actions/)~~
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH-libs/ScanAbs.py.svg)](https://libraries.io/github/KOLANICH-libs/ScanAbs.py)
[![Code style: antiflash](https://img.shields.io/badge/code%20style-antiflash-FFF.svg)](https://codeberg.org/KOLANICH-tools/antiflash.py)

An abstraction layer around port scanners.

Implemented backends:

* [`masscan`](https://github.com/robertdavidgraham/masscan)
* [`zmap`](https://github.com/zmap/zmap)

